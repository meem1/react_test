
import {
  AUTHORIZATION_REQUIRED,
  UNDEFINED_ERROR,
  RESET_UNDEFINED_ERROR,
} from 'constants/Default';

export function getConfig() {
  const config = {
    /*
    headers: {
      Token: localStorage.getItem('token'),
      Apikey: localStorage.getItem('network_api_key'),
    },
    */
  };
  return config;
}

export function commonBackendCall(REQUEST, SUCCESS, FAILURE, requestedAPI) {
  return (dispatch) => {
    dispatch({
      type: REQUEST,
      payload: {
        requesting: true,
      },
    });
    requestedAPI
      .then((response) => {
        dispatch({
          type: SUCCESS,
          payload: {
            requesting: false,
            data: response.data,
          },
        });
      })
      .catch((error) => {
        if (error.response === undefined) {
          dispatch({
            type: UNDEFINED_ERROR,
            payload: {
              error: error.request.status,
            },
          });
          dispatch({
            type: FAILURE,
            payload: {
              requesting: false,
              message: '',
              error_code: '',
            },
          });
        } else if (error.request.status === 401) {
          dispatch({
            type: AUTHORIZATION_REQUIRED,
          });
        } else {
          dispatch({
            type: FAILURE,
            payload: {
              requesting: false,
              message: error.response.data.data.message,
              error_code: error.response.data.data.error_code,
            },
          });
        }
      });
  };
}
