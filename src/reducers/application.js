import {
  POST_APP_DATA_REQUEST,
  POST_APP_DATA_SUCCESS,
  POST_APP_DATA_FAILURE,
  GET_APP_DATA_REQUEST,
  GET_APP_DATA_SUCCESS,
  GET_APP_DATA_FAILURE,
} from '../constants/Application';

const initialState = {
  requesting: false,
  message: '',
  data_list: [],
  refreshApplication: false,
};

export default function applicationsList(state = initialState, action) {
  switch (action.type) {
    case GET_APP_DATA_REQUEST:
      return {
        ...state,
        requesting: action.payload.requesting,
        refreshApplication: false,
      };

    case GET_APP_DATA_SUCCESS:
      return {
        ...state,
        message: action.payload.message,
        requesting: action.payload.requesting,
        data_list: action.payload.data,
      };

    case GET_APP_DATA_FAILURE:
      return {
        ...state,
        requesting: action.payload.requesting,
        message: action.payload.message,
      };

    case POST_APP_DATA_REQUEST:
      return {
        ...state,
        requesting: action.payload.requesting,
      };

    case POST_APP_DATA_SUCCESS:
      return {
        ...state,
        message: action.payload.message,
        requesting: action.payload.requesting,
        refreshApplication: true,
      };

    case POST_APP_DATA_FAILURE:
      return {
        ...state,
        requesting: action.payload.requesting,
        message: action.payload.message,
      };

    default:
      return state;
  }
}
