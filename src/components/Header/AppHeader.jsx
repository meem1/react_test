import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import AddBoxIcon from '@material-ui/icons/AddBox';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));
export default function Header() {

  useEffect(() => {
  }, []);

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar key="header" position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <Link to="/list" style={{ color: '#FFF', hover: '#00F' }}>
              <FormatListBulletedIcon />
            </Link>
          </IconButton>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <Link to="/create" style={{ color: '#FFF', hover: '#00F' }}>
              <AddBoxIcon />
            </Link>
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
}
