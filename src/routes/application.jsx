import ApplicationList from 'views/ApplicationList.jsx';
import ApplicationCreate from 'views/ApplicationCreate.jsx';

const applicationRoutes = [
  {
    path: '/list',
    component: ApplicationList,
  },
  {
    path: '/create',
    component: ApplicationCreate,
  },
  {
    redirect: true, path: '/', to: '/list', navbarName: 'Redirect',
  },
];

export default applicationRoutes;
