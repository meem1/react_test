import Application from 'layouts/Application.jsx';

const indexRoutes = [

  { path: '/', name: 'Home', component: Application },

];
export default indexRoutes;
