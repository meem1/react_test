/* eslint-disable react/display-name */
import React, { useState, useEffect } from 'react';
import MUIDataTable from 'mui-datatables';
import Card from '@material-ui/core/Card';


import { Redirect } from 'react-router-dom';

const ApplicationComp = (props) => {
  //const [dialogOpen, setDialogOpen] = useState(false);
  const [message, setMessage] = useState('');
  const [data, setData] = useState([]);


  const {
    getAppList
  } = props;

  useEffect(() => {
    getAppList()
  }, [getAppList]);

  useEffect(() => {
    setMessage(props.message);
    if (props.refreshApplication) {
      props.getAppList();
    }
    var rowItems = [];
    for (let i = 0; i < props.data_list.length; i++) {
      var colItems = [];
      colItems.push(props.data_list[i].userId);
      colItems.push(props.data_list[i].id);
      colItems.push(props.data_list[i].title);
      colItems.push(props.data_list[i].body);
      rowItems.push(colItems);
    }
    setData(rowItems);
  }, [props]);

  const columns = [
    'UserID',
    'ID',
    'Title',
    'Body',
  ];
  const options = {
    filterType: 'dropdown',
    responsive: 'standard',
    filter: true,
    selectableRows: 'none',
    rowsPerPage: 100,
    selectToolbarPlacement: 'none',
  };

  return (
    <div>
      <Card>

      </Card>
      <Card>
        <MUIDataTable
          title={"Data List"}
          data={data}
          columns={columns}
          options={options}
        />
      </Card>
      <br />
    </div>
  );
};

export default ApplicationComp;
