import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as applicationActions from 'actions/ApplicationActions';
import ApplicationListComp from './ApplicationList/ApplicationListComp';

class ApplicationList extends Component {
  render() {
    const { classes } = this.props;
    const { application, } = this.props;
    const {
      createAppData,
      getAppList
    } = this.props.applicationActions;

    return (
      <div>
        <ApplicationListComp
          createAppData={createAppData}
          getAppList={getAppList}
          refreshApplication={application.refreshApplication}
          data_list={application.data_list}
          requesting={application.requesting}
          message={application.message}
          classes={classes}
        />
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    application: state.application,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    applicationActions: bindActionCreators(applicationActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationList);
