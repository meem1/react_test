/* eslint-disable react/display-name */
import React, { useState, useEffect } from 'react';
import MUIDataTable from 'mui-datatables';
import Card from '@material-ui/core/Card';
import GridItem from 'components/Grid/GridItem.jsx';
import GridContainer from 'components/Grid/GridContainer.jsx';
import Button from '@material-ui/core/Button';
import { useForm, Controller } from "react-hook-form";
import { TextField, Checkbox } from "@material-ui/core";
import { Redirect } from 'react-router-dom';

const ApplicationComp = (props) => {
  const [data, setData] = useState([]);

  const {
    getAppList
  } = props;

  useEffect(() => {
    getAppList()
  }, [getAppList]);

  useEffect(() => {
    if (props.refreshApplication) {
      props.getAppList();
    }
    var rowItems = [];
    for (let i = 0; i < props.data_list.length; i++) {
      var colItems = [];
      colItems.push(props.data_list[i].userId);
      colItems.push(props.data_list[i].id);
      colItems.push(props.data_list[i].title);
      colItems.push(props.data_list[i].body);
      rowItems.push(colItems);
    }
    setData(rowItems);
  }, [props]);

  const columns = [
    'UserID',
    'ID',
    'Title',
    'Body',
  ];
  const options = {
    filterType: 'dropdown',
    responsive: 'standard',
    filter: true,
    selectableRows: 'none',
    rowsPerPage: 100,
    selectToolbarPlacement: 'none',
  };

  const { control, handleSubmit } = useForm();
  const onSubmit = data => {
    props.createAppData({
      userID: data.userID,
      id: data.id,
      title: data.title,
      body: data.body,
    });
  };

  return (
    <div>
      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <Controller
                name="id"
                control={control}
                defaultValue=""
                render={({ field }) =>
                  <TextField
                    fullWidth
                    label="ID"
                    {...field} />}
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Controller
                name="userID"
                control={control}
                defaultValue=""
                render={({ field }) =>
                  <TextField
                    fullWidth
                    label="User ID"
                    {...field} />}
              />
            </GridItem>
          </GridContainer>
          <br />
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Controller
                name="title"
                control={control}
                defaultValue=""
                rules={{ required: true }}
                label="Title"
                render={({ field }) =>
                  <TextField
                    fullWidth
                    required
                    label="Title"
                    {...field} />}
              />
            </GridItem>
          </GridContainer>
          <br />
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Controller
                name="body"
                control={control}
                defaultValue=""
                rules={{ required: true }}
                render={({ field }) =>
                  <TextField
                    fullWidth
                    required
                    label="Body"
                    {...field} />}
              />
            </GridItem>
          </GridContainer>
          <br />
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Button variant="contained"
                style={{ float: 'right' }}
                color="primary"
                type="submit"

              >Create Record</Button>
            </GridItem>
          </GridContainer>
        </form>
      </div>


      <Card>
        <MUIDataTable
          title={"Data List"}
          data={data}
          columns={columns}
          options={options}
        />
      </Card>
      <br />
    </div>
  );
};

export default ApplicationComp;
