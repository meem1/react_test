import API from '../Api/api.js';

import { commonBackendCall, getConfig } from '../Api/common.js';
import {
  POST_APP_DATA_REQUEST,
  POST_APP_DATA_SUCCESS,
  POST_APP_DATA_FAILURE,
  GET_APP_DATA_REQUEST,
  GET_APP_DATA_SUCCESS,
  GET_APP_DATA_FAILURE,
} from '../constants/Application';

export function createAppData(obj) {
  return commonBackendCall(
    POST_APP_DATA_REQUEST,
    POST_APP_DATA_SUCCESS,
    POST_APP_DATA_FAILURE,
    API.post('posts', obj, getConfig()),
  );
}

export function getAppList() {
  return commonBackendCall(
    GET_APP_DATA_REQUEST,
    GET_APP_DATA_SUCCESS,
    GET_APP_DATA_FAILURE,
    API.get(`posts`, getConfig()),
  );
}

