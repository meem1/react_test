import React /* ,{useState, useEffect } */ from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import AppHeader from 'components/Header/AppHeader.jsx';
import applicationRoutes from 'routes/application';
import PropTypes from 'prop-types';
import { createStyles } from '@material-ui/styles'

const styles = createStyles(() => ({
  wrapper: {
    height: 'auto',
    minHeight: '100vh',
    position: 'relative',
    top: '0',
    overflow: 'hidden',
  },
  mainPanelUser: {
    width: 'calc(100%)',
    overflow: 'auto',
    position: 'relative',
    float: 'right',
    maxHeight: '100%',
    width: '100%',
    overflowScrolling: 'touch',
  },
  content: {
    marginTop: '10px',
    padding: '10px',
    minHeight: 'calc(100vh - 123px)',
  },
}));
function AuthenticatedRoute({ component: Comp, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => (
        <Comp {...props} />
      )}
    />
  );
}

function Application({ ...props }) {
  const { classes } = props;
  return (
    <div className={classes.wrapper} >
      <div className={classes.mainPanelUser}>
        <AppHeader />
        <div className={classes.content}>
          <Switch>
            {applicationRoutes.map((prop, key) => {
              if (prop.redirect) return <Redirect from={prop.path} to={prop.to} key={key} />;
              return (
                <AuthenticatedRoute
                  path={prop.path}
                  component={prop.component}
                  key={key}
                />
              );
            })}
          </Switch>
        </div>
      </div>
    </div>
  );
}

Application.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Application);


